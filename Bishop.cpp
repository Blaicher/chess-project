#include <string>
#include <iostream>
#include <cmath>
#include "Bishop.h"

Bishop::Bishop(char color)
{
	this->color = color;
}
int Bishop::isLegal(string move, Board boardPtr) const
{
	const int fromRow = 0;
	const int toRow = 2;
	const int fromCol = 1;
	const int toCol = 3;
	int j = 0;
	int* Mpositions = Board::getPositions(move); // Getting the positions in the array according to string 'move'
	int positions[4] = { Mpositions[0], Mpositions[1], Mpositions[2], Mpositions[3] };
	int curPosition[2] = { 0 };
	int lowerPosition[2] = { 0 };
	int higherPosition[2] = { 0 };
	int toPositions[2] = { positions[toRow], positions[toCol] };

	if (abs(positions[fromRow] - positions[toRow]) == abs(positions[fromCol] - positions[toCol])) // Checking if the move is valid
	{
		//Checking if we are trying to capture a piece of the same color:
		if (boardPtr.checkPosition(toPositions)->color == this->color)
		{
			return INVALID_DESTINATION_COLOR;
		}
		//Checking that nothing is in the way:
		if (positions[fromRow] < positions[toRow] && positions[fromCol] > positions[toCol])
		{
			j = positions[fromCol];
			for (int i = positions[fromRow]; i < positions[toRow]; i++)
			{
				//Lower the file and up the rows:
				if (i != positions[fromRow]) //Ignore the first run to avoid the moving piece while checking the way
				{
					curPosition[0] = i;
					curPosition[1] = j;
					Piece* curPiece = boardPtr.checkPosition(curPosition);
					if (curPiece->type.compare("Empty") && curPosition[0] != toPositions[0] && curPosition[1] != toPositions[1]) // Something is in the way!
					{
						return INVALID_MOVE;
					}
				}
				j--;
			}
			return VALID;
		}
		else if (positions[fromRow] > positions[toRow] && positions[fromCol] < positions[toCol])
		{
			j = positions[fromRow];
			for (int i = positions[fromCol]; i < positions[toCol]; i++)
			{
				//Up the file and lower the rows:
				if (i != positions[fromRow]) //Ignore the first run to avoid the moving piece while checking the way
				{
					curPosition[0] = i;
					curPosition[1] = j;
					Piece* curPiece = boardPtr.checkPosition(curPosition);
					if (curPiece->type.compare("Empty") && curPosition[0] != toPositions[0] && curPosition[1] != toPositions[1]) // Something is in the way!
					{
						return INVALID_MOVE;
					}
				}
				j--;
			}
			return VALID;

		}

		else if (positions[fromRow] > positions[toRow] && positions[fromCol] > positions[toCol])
		{
			j = positions[fromCol];
			for (int i = positions[fromRow]; i > positions[toRow]; i--)
			{
				//Lower the files and the rows:
				if (i != positions[fromRow]) //Ignore the first run to avoid the moving piece while checking the way
				{
					curPosition[0] = i;
					curPosition[1] = j;
					Piece* curPiece = boardPtr.checkPosition(curPosition);
					if (curPiece->type.compare("Empty") && curPosition[0] != toPositions[0] && curPosition[1] != toPositions[1]) // Something is in the way!
					{
						return INVALID_MOVE;
					}
				}
				j--;
			}
			return VALID;
		}
		else if (positions[fromRow] < positions[toRow] && positions[fromCol] < positions[toCol])
		{
			j = positions[fromRow];
			for (int i = positions[fromCol]; i < positions[toCol]; i++)
			{
				//Lower the files and the rows:
				if (j != positions[fromRow]) //Ignore the first run to avoid the moving piece while checking the way
				{
					curPosition[0] = j;
					curPosition[1] = i;
					Piece* curPiece = boardPtr.checkPosition(curPosition);
					if (curPiece->type.compare("Empty") && curPosition[0] != toPositions[0] && curPosition[1] != toPositions[1]) // Something is in the way!
					{
						return INVALID_MOVE;
					}
				}
				j++;
			}
			return VALID;
		}
		else
		{
			return INVALID_MOVE;
		}
	}
	else
	{
		return INVALID_MOVE;
	}
}