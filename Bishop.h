#pragma once
#include "Board.h"
#include "Piece.h"
class Bishop : public Piece
{
public:
	Bishop(char color);
	virtual int isLegal(string move, Board boardPtr) const;
	string type = "Bishop";
	char color;
	char sign = 'B';
};