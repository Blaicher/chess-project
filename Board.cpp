#include "Board.h"
#include "Game.h"
#include "Piece.h"
#include "Knight.h"
#include "Bishop.h"
#include "Rook.h"
#include "King.h"
#include "Pawn.h"
#include "Queen.h"
#include "Empty.h"
enum files {a,b,c,d,e,f,g,h};
Board::Board()
{
	for (int i = 0; i < CHESSBOARD_SIZE; i++)
	{
		
		this->_board[1][i] = new Pawn('W');
		this->_board[1][i]->setPiece('W', 'P', "Pawn");
		this->_board[6][i] = new Pawn('B');
		this->_board[6][i]->setPiece('B', 'p', "Pawn");
	}
	
	for (int i = 2; i < CHESSBOARD_SIZE - 2; i++)
	{
		for (int j = 0; j < CHESSBOARD_SIZE; j++)
		{
			this->_board[i][j] = new Empty('#');
			this->_board[i][j]->setPiece('#', '#', "Empty");
		}
	}

	this->_setRooks();
	this->_setKnights();
	this->_setBishops();
	this->_setQueens();
	this->_setKings();
}
int* Board::getKingPosition()
{
	int positions[4] = { -1 };
	int count = 0;
	for (int i = 0; i < CHESSBOARD_SIZE; i++)
	{
		for (int j = 0; j < CHESSBOARD_SIZE; j++)
		{
			if (!(this->_board[i][j])->type.compare("King"))
			{
				if (count == 0)
				{
					positions[0] = i;
					positions[1] = j;
					count++;
				}
				else if (count == 1)
				{
					positions[2] = i;
					positions[3] = j;
				}
				
			}
		}
	}
	return positions;
}



void Board::_setRooks()
{
	
	this->_board[0][0] = new Rook('W');
	this->_board[0][0]->setPiece('W', 'R', "Rook");
	this->_board[0][7] = new Rook('W');
	this->_board[0][7]->setPiece('W', 'R', "Rook");
	this->_board[7][0] = new Rook('B');
	this->_board[7][0]->setPiece('B', 'r', "Rook");
	this->_board[7][7] = new Rook('B');
	this->_board[7][7]->setPiece('B', 'r', "Rook");
}


void Board::_setKnights()
{
	
	this->_board[0][1] = new Knight('W');
	this->_board[0][6] = new Knight('W');
	this->_board[0][1]->setPiece('W', 'N', "Knight");
	this->_board[0][6]->setPiece('W', 'N', "Knight");
	
	this->_board[7][1] = new Knight('B');
	this->_board[7][1]->setPiece('B', 'n', "Knight");
	this->_board[7][6] = new Knight('B');
	this->_board[7][6]->setPiece('B', 'n', "Knight");
}

void Board::_setBishops()
{
	
	this->_board[0][2] = new Bishop('W');
	this->_board[0][2]->setPiece('W', 'B', "Bishop");
	this->_board[0][5] = new Bishop('W');
	this->_board[0][5]->setPiece('W', 'B', "Bishop");
	
	this->_board[7][2] = new Bishop('B');
	this->_board[7][2]->setPiece('B', 'b', "Bishop");
	this->_board[7][5] = new Bishop('B');
	this->_board[7][5]->setPiece('B', 'b', "Bishop");
}

void Board::_setQueens()
{
	this->_board[0][3] = new Queen('W');
	this->_board[7][3] = new Queen('B');
	this->_board[0][3]->setPiece('W', 'Q', "Queen");
	this->_board[7][3]->setPiece('B', 'q', "Queen");
}


void Board::_setKings()
{
	
	this->_board[0][4] = new King('W');
	this->_board[7][4] = new King('B');
	this->_board[0][4]->setPiece('W', 'K', "King");
	this->_board[7][4]->setPiece('B', 'k', "King");
}

void Board::printBoard()
{
	for (int i = CHESSBOARD_SIZE - 1; i >= 0; i--)
	{
		for (int j = 0; j < CHESSBOARD_SIZE; j++)
		{
			std::cout << " " << this->_board[i][j]->sign << " ";
		}
		std::cout << std::endl;
	}

	
}


int* Board::getPositions(string move)
{
	int positions[MOVE_LENGTH];
	unsigned int j = 0;
	positions[0] = (int)move[1] - '1';
	positions[2] = (int)move[3] - '1';
	for (int i = 1; i < MOVE_LENGTH; i += 2)
	{
		switch (move[j])
		{
		case 'a':
			positions[i] = a;
			break;
		case 'b':
			positions[i] = b;
			break;
		case 'c':
			positions[i] = c;
			break;
		case 'd':
			positions[i] = d;
			break;
		case 'e':
			positions[i] = e;
			break;
		case 'f':
			positions[i] = f;
			break;
		case 'g':
			positions[i] = g;
			break;
		case 'h':
			positions[i] = h;
			break;
		default:
			positions[i] = -1;
			break;
		}
		j += 2;
	}
	return positions;
}




Piece* Board::checkPosition(int* position) const
{
	return this->_board[position[0]][position[1]];
}


/*
The function creates a move command using the positions it gets and the size it gets as well.
input: the positions array and the size.
output: the move command by the positions we get in the parameters.
*/
string Board::createMoveCommandByPositions(int* positions, int size) const
{
	string move = ""; //The varriable to save the command string type.
	for (int i = 0; i < size; i++)
	{
		if (i % 2 == 0)
		{
			switch (positions[i + 1])
			{
			case 0:
				move += 'a';
				break;
			case 1:
				move += 'b';
				break;
			case 2:
				move += 'c';
				break;
			case 3:
				move += 'd';
				break;
			case 4:
				move += 'e';
				break;
			case 5:
				move += 'f';
				break;
			case 6:
				move += 'g';
				break;
			case 7:
				move += 'h';
				break;

			default:
				break;
			}

		}
		else
		{
			move += positions[i - 1] + '1';
		}
	}
	return move;
}
int Board::move(string move, char color)
{
	int* position = this->getPositions(move);
	int fromRow = position[0];
	int fromCol = position[1];
	int toRow = position[2];
	int toCol = position[3];
	char oppColor = 'W';
	if (fromRow == toRow && fromCol == toCol) // Movigng to the same square
	{
		return SAME_SQUARE;
	}
	if (this->_board[fromRow][fromCol]->color != color)
	{
		return INVALID_COLOR;
	}
	Board c = *this;
	if (color == 'W')
	{
		oppColor = 'B';
	}
	int code = this->_board[fromRow][fromCol]->isLegal(move, c);
	if (code == VALID)
	{
			delete this->_board[toRow][toCol];
			this->_board[toRow][toCol] = this->_board[fromRow][fromCol];
			this->_board[fromRow][fromCol] = new Empty('#');
			this->_board[fromRow][fromCol]->setPiece('#', '#', "Empty");
			if (!this->_board[toRow][toCol]->type.compare("King"))
			{
				if (this->_board[toRow][toCol]->color == 'W')
				{
					this->hasWhiteKingMoved = true;
				}
				else
				{
					this->hasBlackKingMoved = true;
				}
			}

			if (this->findKing(color)) // Move made a check to our own king!
			{
				delete this->_board[fromRow][fromCol];
				this->_board[fromRow][fromCol] = this->_board[toRow][toCol];
				this->_board[toRow][toCol] = new Empty('#');
				this->_board[toRow][toCol]->setPiece('#', '#', "Empty");
				return SELF_CHECK;
			}
			
			else if(this->findKing(oppColor)) // The player made a check to the enemy!
			{
				return CHECK;
			}
			return VALID;
	}
	else
	{
		return code;
	}
}



Board& Board::operator=(const Board& from)
{
	char cur;
	for (int i = 0; i < CHESSBOARD_SIZE; i++)// Running for the whole board
	{
		for (int j = 0; j < CHESSBOARD_SIZE; j++)
		{
			cur = from._board[i][j]->sign;
			switch (cur)
			{
			case 'P':
				this->_board[i][j] = new Pawn('W');
				this->_board[i][j]->setPiece('W', 'P', "Pawn");
				break;
			case 'p':
				this->_board[i][j] = new Pawn('B');
				this->_board[i][j]->setPiece('B', 'p', "Pawn");
				break;
			case 'R':
				this->_board[i][j] = new Rook('W');
				this->_board[i][j]->setPiece('W', 'R', "Rook");
				break;
			case 'r':
				this->_board[i][j] = new Rook('B');
				this->_board[i][j]->setPiece('B', 'r', "Rook");
				break;
			case 'B':
				this->_board[i][j] = new Bishop('W');
				this->_board[i][j]->setPiece('W', 'B', "Bishop");
				break;
			case 'b':
				this->_board[i][j] = new Bishop('B');
				this->_board[i][j]->setPiece('B', 'b', "Bishop");
				break;
			case 'N':
				this->_board[i][j] = new Knight('W');
				this->_board[i][j]->setPiece('W', 'N', "Knight");
				break;
			case 'n':
				this->_board[i][j] = new Knight('B');
				this->_board[i][j]->setPiece('B', 'n', "Bishop");
				break;
			case 'Q':
				this->_board[i][j] = new Queen('W');
				this->_board[i][j]->setPiece('W', 'Q', "Queen");
				break;
			case 'q':
				this->_board[i][j] = new Queen('B');
				this->_board[i][j]->setPiece('B', 'q', "Queen");
				break;
			case 'K':
				this->_board[i][j] = new King('W');
				this->_board[i][j]->setPiece('W', 'K', "King");
				break;
			case 'k':
				this->_board[i][j] = new King('B');
				this->_board[i][j]->setPiece('B', 'k', "King");
				break;
			default: // Is empty
				this->_board[i][j] = new Empty('#');
				this->_board[i][j]->setPiece('#', '#', "Empty");
				break;
			}
			
		}
	}
	return *this;
}

/*
The function returns the board piece by the move string it gets.
input: move string.
output: a pointer to piece object that is described in the move command string.
*/
Piece* Board::getBoardPieceByMoveCommand(string move) const
{
	//Getting the values into varriables.
	int line = 1;
	int row = move[1] - '1'; //Getting the integer value of the row.
	int i = 0; //Loup varriable.
	bool found = false;
	for (i = 0; i < 8 && !found; i++)
	{
		//If we got to our line value.
		if (boardLines[i] == move[0])
		{
			found = true;
		}
		else
		{
			line++;
		}
	}
	
	return _board[row - 1][line - 1];
}



bool Board::sourceSquareIsInTheSameColor(string move, char current_col) const
{
	int row = move[1] - '0'; //Getting the index of the row.
	int line = 1; //The varriable of the source line of the move command.
	bool found = false; //Points on the fact wheather we found the index or not.
	int i = 0; //Loup varriables.
	Piece* currentPiece; //This is the varriable to keep the piece we got to.
	for (i = 0; i < 8 && !found; i++)
	{
		if (boardLines[i] == move[0])
		{
			//Checking if it is this line.
			found = true;
		}
		else {
			line++;
		}
	}
	currentPiece = this->_board[line][row]; //Getting the current piece we want.
	return currentPiece->color == current_col;
}

void Board::setPiece(int* position, Piece* toPut)
{
	const int i = position[0];
	const int j = position[1];
	delete this->_board[i][j];
	this->_board[i][j] = toPut;
}


bool Board::getKingStatus(char color) const
{
	if (color == 'W')
	{
		return this->hasWhiteKingMoved;
	}
	else
	{
		return this->hasBlackKingMoved;
	}
}





bool Board::findKing(char color)
{
	int* mpositions; //The positions array of the king.
	mpositions = this->getKingPosition(); //Getting the king postions.
	int positions[SIZE] = { mpositions[FIRST_INDEX], mpositions[SECOND_INDEX], mpositions[THIRD_INDEX], mpositions[FOURTH_INDEX] };
	int i = 0; //Loup varriable.
	int j = 0; //Loup varriable.
	Board newBoard = *this; //Getting the current board into a new one.
	//Saving the values of the positions in varriables:
	int firstKing[SIZE_MINI_ARR] = { positions[FIRST_INDEX],positions[SECOND_INDEX] }; //The small array of only the position of the first king.
	int secondKing[SIZE_MINI_ARR] = { positions[THIRD_INDEX],positions[FOURTH_INDEX] };//The same only for the second king.
	Piece* currentPiece = 0; //The piece savior varriable.
	Piece* firstKingPiece = newBoard.checkPosition(firstKing); //Getting the first king Piece object using the function.
	Piece* secondKingPiece = newBoard.checkPosition(secondKing); //The same to the second one.
	for (i = 0; i < CHESSBOARD_SIZE; i++)
	{
		for (j = 0; j < CHESSBOARD_SIZE; j++)
		{
			int helpArray[SIZE_MINI_ARR] = { i,j }; //Getting the current piece position.
			currentPiece = newBoard.checkPosition(helpArray); //Getting the current piece.
			//Checking first of all if the current piece we get using the function has different color than one of the kings and than we can continue.
			if (firstKingPiece->color != currentPiece->color && firstKingPiece->color == color && currentPiece->color != '#')
			{
				int helpPositions[SIZE] = { i,j,positions[FIRST_INDEX],positions[SECOND_INDEX] }; //Building the array.
				string move = newBoard.createMoveCommandByPositions(helpPositions, SIZE); //Getting the move.
				int moveSuccesful = currentPiece->isLegal(move, newBoard);
				if (moveSuccesful == VALID)
				{
					return true;
				}
			}
			else if (secondKingPiece->color != currentPiece->color && secondKingPiece->color == color)
			{
				int helpPositions[SIZE] = { i,j,positions[THIRD_INDEX],positions[FOURTH_INDEX] }; //Building the array.
				string move = newBoard.createMoveCommandByPositions(helpPositions, SIZE); //Getting the move.
				int moveSuccesful = currentPiece->isLegal(move, newBoard);
				if (moveSuccesful == VALID)
				{
					return true;
				}
			}
		}
	}
	return false;
}



/*
The function checks if the king in the color parameter it gets is in
mat situation.
input: color character.
output : true if it is and false if it is not.
*/
bool Board::checkMatSituation(char color)
{
	
	
	int fromPosition[2] = { 0 };
	int toPosition[2] = { 0 };
	Board a;
	a = *this;
	Piece* curPiece;
	for (int i = 0; i < CHESSBOARD_SIZE; i++)
	{
		for (int j = 0; j < CHESSBOARD_SIZE; j++)
		{
			fromPosition[0] = i;
			fromPosition[1] = j;
			curPiece = a.checkPosition(fromPosition);
			for (int k = 0; k < CHESSBOARD_SIZE; k++)
			{
				for (int l = 0; l < CHESSBOARD_SIZE; l++)
				{
					toPosition[0] = k;
					toPosition[1] = l;
					int positions[4] = { fromPosition[0], fromPosition[1], toPosition[0], toPosition[1] };
					string move = createMoveCommandByPositions(positions, SIZE);
					int answer = a.move(move, color);
					if (answer == VALID)
					{
						return false; // There is no mate!
					}
				}
			}
		}
	}
	return true; // A checkmate is on the board!
}