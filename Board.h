#pragma once

#include "Piece.h"
#include <string>
#include <iostream>
#define CHESSBOARD_SIZE 8
#define MOVE_LENGTH 4
#define SIZE 4
#define STARTING_VALUE -1

#define FIRST 0
#define SECOND 1
#define THIRD 2
#define FOURTH 3
using std::string;
class Piece;
class Board 
{
public:
	Board(); // Function to fill _board
	void printBoard(void);
	int* getKingPosition();
	Piece* getBoardPieceByMoveCommand(string move) const;
	int move(string move, char color);
	void setPiece(int* position, Piece* toPut);
	bool findKing(char color);
	bool checkMatSituation(char color);
	bool getKingStatus(char color) const;
	static int* getPositions(string move);
	bool sourceSquareIsInTheSameColor(string move, char current_col) const;
	Piece* checkPosition(int* positions) const;
	bool isKingInCheck(char kingColor);
	string createMoveCommandByPositions(int* positions, int size) const;
	Board& operator=(const Board& from);
	
private:
	Piece* _board[CHESSBOARD_SIZE][CHESSBOARD_SIZE];
	char boardLines[8] = { 'a','b','c','d','e','f','g','h' }; //All the characters that the board has in its rows.
	void _setRooks();
	void _setKnights();
	void _setBishops();
	void _setQueens();
	void _setKings();
	bool hasWhiteKingMoved = false;
	bool hasBlackKingMoved = false;
};