#pragma once
#include "Piece.h"

class Empty : public Piece
{
public:
	Empty(char color)
	{
		this->color = color;
	}

	virtual int isLegal(string move, Board boardPtr) const  { return INVALID_MOVE; }
	char color = '#';
	string type = "Empty";
	char sign = '#';

};