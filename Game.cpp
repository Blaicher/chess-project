#include <iostream>
#include <string>
#include <string.h>

#include "Game.h"
using std::string;
Game::Game(Board boardPtr)
{
	this->_boardPtr = boardPtr;
}

int Game::move(string move)
{
	int ans = this->_boardPtr.move(move, this->_turn);
	char oppColor = 'W';
	if (this->_turn == 'W')
	{
		oppColor = 'B';
	}
	if (ans < 2) // is a valid move!
	{
		this->changeTurn();
		
	}
	return ans;
}

Board* Game::getBoard()
{
	return &this->_boardPtr;
}


void Game::changeTurn(void)
{
	if (this->_turn == 'W')
	{
		this->_turn = 'B';
	}
	else
	{
		this->_turn = 'W';
	}
}
bool Game::findKing()
{
	Board newBoard = this->_boardPtr;
	char indexesLines[8] = { 'a','b','c','d','e','f','g','h' };
	char indexesRow[8] = { '1','2','3','4','5','6','7','8' };
	int* kingPosition = _boardPtr.getKingPosition(); //Getting the king position.
	int kingPositions[4] = { kingPosition[0], kingPosition[1], kingPosition[2], kingPosition[3] };
	int i = 0; //Loup varriable.
	int j = 0; //Loup varriable.
	for (i = 0; i < CHESSBOARD_SIZE; i++)
	{
		for (j = 0; j < CHESSBOARD_SIZE; j++)
		{
			string source = "";
			source = source + indexesLines[i];
			source = source + indexesRow[j];
			Piece* currentPiece = newBoard.getBoardPieceByMoveCommand(source);
			string command = "";
			command = command + source;
			string dest = "";
			dest = dest + indexesLines[kingPositions[0]];
			dest = dest + indexesRow[kingPositions[1]];
			command = command + dest;
			if (currentPiece->isLegal(command, newBoard))
			{
				return true;
			}
		}
	}
	return false;
}



char Game::getCurrentTurn()
{
	return this->_turn;
}