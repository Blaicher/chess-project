#pragma once
#include <iostream>
#include <string>
#include "Board.h"
#define SIZE_ARR 4
#define FIRST_INDEX 0
#define SECOND_INDEX 1
#define THIRD_INDEX 2
#define FOURTH_INDEX 3
#define SIZE_MINI_ARR 2

class Game 
{
public:
	Game(Board boardPtr);
	bool findKing();
	int move(string move);
	void changeTurn(void);
	Board* getBoard();
	char getCurrentTurn();
private:
	Board _boardPtr;
	char _turn = 'W';
};