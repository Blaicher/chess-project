#include "King.h"

King::King(char color)
{
	this->color = color;
}

int King::isLegal(string move, Board boardPtr) const
{
	const int fromRow = 0;
	const int toRow = 2;
	const int fromCol = 1;
	const int toCol = 3;
	int* positions = Board::getPositions(move); // Getting the positions in the array according to string 'move'
	int toPositions[2] = { positions[toRow], positions[toCol] };
	int curPosition[2] = { 0 };

	if ((positions[fromCol] == positions[toCol]) && (positions[fromRow] + 1 == positions[toRow]) ||
		positions[fromCol] + 1 == positions[toCol] && (positions[fromRow] == positions[toRow]) ||
		positions[fromCol] + 1 == positions[toCol] && (positions[fromRow] + 1 == positions[toRow]) ||
			(positions[fromCol] - 1 == positions[toCol]) && (positions[fromRow] == positions[toRow]) ||
		(positions[fromCol] == positions[toCol]) && (positions[fromRow] - 1 == positions[toRow]) ||
		(positions[fromCol] - 1 == positions[toCol]) && (positions[fromRow] - 1 == positions[toRow]) ||
		(positions[fromCol] + 1 == positions[toCol]) && (positions[fromRow] - 1 == positions[toRow]) ||
		(positions[fromCol] - 1 == positions[toCol]) && (positions[fromRow] + 1 == positions[toRow]))
	{
		if (boardPtr.checkPosition(toPositions)->color != this->color)
		{
			
			return VALID;
		}
		else
		{
			return INVALID_DESTINATION_COLOR;
		}
	}
	else
	{
		return INVALID_MOVE;
	}
}


