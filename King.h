#pragma once
#include "Piece.h"

class King : public Piece
{
public:
	King(char color);
	virtual int isLegal(string move, Board boardPtr) const;
	string type = "King";
	char color;
	char sign = 'K';

};
