#include "Pawn.h"
#include "Board.h"
Pawn::Pawn(char color)
{
	this->color = color;
}
int Pawn::isLegal(string move, Board boardPtr) const
{
	const int fromRow = 0;
	const int toRow = 2;
	const int fromCol = 1;
	const int toCol = 3;
	int calcPosition = 0;
	if (this->color == 'W')
	{
		
		calcPosition = 1;
	}
	else // Is a black peice!
	{
		calcPosition = -1;
	}
	int* positions = Board::getPositions(move); // Getting the positions in the array according to string 'move'
	
	
	int toPosition[2] = { positions[toRow], positions[toCol] };
	int inWay[2] = { toPosition[0] - calcPosition, toPosition[1]  };
	
	//Moving forward:
	if (positions[fromRow] + calcPosition == positions[toRow] && positions[fromCol] == positions[toCol]) // Normal movement
	{
		
		//Checking if the position is empty!
		Piece* cur = boardPtr.checkPosition(toPosition);
		if (!(cur->type.compare("Empty"))) // Place is empty!
		{
			return VALID;
		}
		else
		{
			
			return INVALID_MOVE;
		}
	}

	//Capturing:
	else if (positions[fromRow] + calcPosition == positions[toRow] && positions[fromCol] == positions[toCol] + calcPosition ||
		positions[fromRow] == positions[toRow] - calcPosition && positions[fromCol] == positions[toCol] - calcPosition) // Valid capture move!
	{
		Piece* cur = boardPtr.checkPosition(toPosition);
		if (!cur->type.compare("Empty"))
		{
			return INVALID_MOVE;
		}
		else if (cur->color != this->color) // Cannot capture pieces of your own color!
		{
			return VALID;
		}
		else
		{
			return INVALID_DESTINATION_COLOR;
		}
	}

	//Special move first time!
	if ((positions[fromRow] + toRow == positions[toRow] && (positions[fromRow] == 1 && this->color == 'W') || (positions[fromRow] == 6 && this->color == 'B')) && positions[fromCol] == positions[toCol])// First move of the pawn can be two squeres forward!
	{
		 if (positions[toCol] == positions[fromCol])
		 {
			 
		 }
		Piece* cur = boardPtr.checkPosition(toPosition); // Checking the needed squere..
		
		if (!(cur->type.compare("Empty"))) // Place is empty!
		{
			
			cur = boardPtr.checkPosition(inWay);
			
			if (!(cur->type.compare("Empty")))
			{
				return VALID; // Legal move!
			}
			else
			{
				
				return INVALID_MOVE;
			}
		}
		else
		{
			
			return INVALID_MOVE;
		}
	}


	
	else
	{
		
		return INVALID_MOVE;
	}
}