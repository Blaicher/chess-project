#pragma once
#include <string>
#include "Board.h"
#define VALID 0
#define CHECK 1
#define INVALID_COLOR 2
#define INVALID_DESTINATION_COLOR 3
#define SELF_CHECK 4
// Error code 5 is unneeded
#define INVALID_MOVE 6
#define SAME_SQUARE 7
#define CHECKMATE 8
class Board;
using std::string;
class Piece
{
public:
	virtual int isLegal(string move, Board boardPtr) const = 0;
	string type;
	char color;
	char sign;
	void setPiece(char color, char sign, string type);
};