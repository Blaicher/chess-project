#include "Queen.h"
#include "Rook.h"
#include "Bishop.h"
Queen::Queen(char color)
{
	this->color = color;
}
int Queen::isLegal(string move, Board boardPtr) const
{
	Board a;
	Board b;
	a = boardPtr;
	b = boardPtr;
	Piece* first = new Rook(this->color);
	Piece* second = new Bishop(this->color);
	first->setPiece(this->color, 'R', "Rook");
	second->setPiece(this->color, 'B', "Bishop");
	int* Mpositions = a.getPositions(move);
	int positions[4] = { Mpositions[0], Mpositions[1], Mpositions[2], Mpositions[3] };
	int fromPositions[2] = { positions[0], positions[1] };
	int fromPositionsM[2] = { positions[0], positions[1] };
	a.setPiece(fromPositions, first);
	b.setPiece(fromPositionsM, second);
	int firstAnswer = first->isLegal(move, a);
	int secondAnswer = second->isLegal(move, b);
	if (first->isLegal(move, a) == VALID || second->isLegal(move, b) == VALID)
	{
		return VALID;
	}
	else
	{
		if (firstAnswer != INVALID_MOVE) // Checking for special reason for failure:
		{
			return firstAnswer;
		}
		else if (secondAnswer != INVALID_MOVE)
		{
			return secondAnswer;
		}
		else
		{
			return INVALID_MOVE;
		}
	}
}