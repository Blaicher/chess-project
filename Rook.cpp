#include "Rook.h"

Rook::Rook(char color)
{
	this->color = color;
}

int Rook::isLegal(string move, Board boardPtr) const
{
	const int fromRow = 0;
	const int toRow = 2;
	const int fromCol = 1;
	const int toCol = 3;
	int side = 1;
	int curPosition[2] = { 0, 0 };
	Piece* cur;
	int* Mpositions = Board::getPositions(move); // Getting the positions in the array according to string 'move'
	int positions[4] = { Mpositions[0], Mpositions[1], Mpositions[2], Mpositions[3] };
	int toPosition[2] = { positions[toRow], positions[toCol] };
	
	if (positions[fromRow] == positions[toRow] || positions[toCol] == positions[fromCol]) //Checking if the move is legal only by move
	{
		//Checking if the square is not occupied by a peice of the same color:
		if (boardPtr.checkPosition(toPosition)->color == this->color)
		{
			return INVALID_DESTINATION_COLOR;
		}
		
		//Checking whether the path of movement is clear:
		if (positions[fromCol] == positions[toCol])
		{
			if (positions[fromRow] > positions[toRow]) // Movement is backwards
			{
				side = -1;
				for (int i = positions[fromRow]; i > positions[toRow]; i += side) // Running for all the possible positions on the way
				{
					if (i != positions[fromRow]) // Ignoring first run so we don't check the moving piece
					{
						curPosition[0] = i;
						curPosition[1] = positions[toCol];
						cur = boardPtr.checkPosition(curPosition);

						if (cur->type.compare("Empty")) // Something is in the way!
						{
							return INVALID_MOVE;
						}
					}
				}
				//Nothing is in the way
				return VALID;
			}
			for (int i = positions[fromRow]; i < positions[toRow]; i += side) // Running for all the possible positions on the way
			{
				if (i != positions[fromRow]) // Ignoring first run so we don't check the moving piece
				{
					curPosition[0] = i;
					curPosition[1] = positions[toCol];
					cur = boardPtr.checkPosition(curPosition);

					if (cur->type.compare("Empty")) // Something is in the way!
					{
						return INVALID_MOVE;
					}
				}
			}
			//Nothing is in the way
			return VALID;
			
			
		}
		else
		{
			//Checking whether the path of movement is clear:
			
				if (positions[fromCol] > positions[toCol]) // Movement is backwards
				{
					side = -1;
					for (int i = positions[fromRow]; i > positions[toRow]; i += side) // Running for all the possible positions on the way
					{
						if (i != positions[fromRow]) // Ignoring first run so we don't check the moving piece
						{
							curPosition[0] = i;
							curPosition[1] = positions[toCol];
							cur = boardPtr.checkPosition(curPosition);

							if (cur->type.compare("Empty")) // Something is in the way!
							{
								return INVALID_MOVE;
							}
						}
					}
					//Nothing is in the way
					return VALID;
				}
				for (int i = positions[fromCol]; i < positions[toCol]; i += side) // Running for all the possible positions on the way
				{
					if (i != positions[fromCol]) // Ignoring first run so we don't check the moving piece
					{
						curPosition[0] = positions[toRow];
						curPosition[1] = i;
						cur = boardPtr.checkPosition(curPosition);
						
						if (cur->type.compare("Empty")) // Something is in the way!
						{
							return INVALID_MOVE;
						}
					}
				}
				//Nothing is in the way
				return VALID;
		}
	}
	else
	{
		return INVALID_MOVE;
	}
}