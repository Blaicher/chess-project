#pragma once
#include "Piece.h"
class Rook : public Piece
{
public:
	Rook(char color);
	virtual int isLegal(string move, Board boardPtr) const;
	
	string type = "Rook";
	char color;
	char sign = 'R';
	bool hasMoved = false;
};