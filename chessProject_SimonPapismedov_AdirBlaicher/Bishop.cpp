#include <string>
#include <iostream>
#include "Bishop.h"
using namespace std;
Bishop::Bishop(char color)
{
	this->color = color;
}
bool Bishop::isLegal(string move, Board boardPtr) const
{
	//Varriable to save the current piece we are dealling with:
	Piece* currentPiece = 0;
	//Helping varriables:
	char saveFirstLine = 'a';
	char saveSecondLine = 'a';
	char saveFirstRow = 'a';
	char saveSecondRow = 'a';
	//Getting the information of the move command.
	char firstLine = move[0];
	char secondLine = move[2];
	char firstRow = move[1];
	char secondRow = move[3];
	//
	string new_move = ""; //Keeps the second half string.
	//Start checking if the move is llegel.
	bool isInTheBoard = this->isOnLegelIndex(move); //Checking if the move is one the good places of the board.
	if (!isInTheBoard)
	{
		return false;
	}
	//Getting the new move string and sending to the function for checking if the destination square is not our color type of piece.
	new_move = move[2] + move[3];
	if (boardPtr.sourceSquareIsInTheSameColor(new_move, this->color) == true)
	{
		return false;
	}
	//Checking if the Bishop source and destination is fine.
	if(!( (secondLine - firstLine == secondRow - firstRow) || (firstLine - secondLine == secondRow - firstRow) || 
		(secondLine - firstLine ==  firstRow - secondRow) || (firstLine -secondLine == firstRow - secondRow)))
	{
		return false;
	}
	///Checking if there is a clean path for the bishop to move.
	if ( secondLine > firstLine)
	{
		//Saving the varriables:
		saveFirstLine = firstLine;
		saveSecondLine = secondLine;
		saveFirstRow = firstRow;
		saveSecondRow = secondRow;
		while (saveFirstLine != saveSecondLine)
		{
			//Upgrading the save varriables' value and check wheather the board has a clean space there.
			saveFirstLine++;
			saveFirstRow++;
			string command="";
			command = command + saveFirstLine;
			command = command + saveFirstRow;
			currentPiece = boardPtr.getBoardPieceByMoveCommand(command); //Sending to the function in order to get the piece.
			if (currentPiece->color == this->color)
			{
				return false;
			}
			if (currentPiece->type != "Empty")
			{
				return false;
			}
		}
	}
	else
	{
		//Saving the varriables:
		saveFirstLine = firstLine;
		saveSecondLine = secondLine;
		saveFirstRow = firstRow;
		saveSecondRow = secondRow;
		while (saveFirstLine != saveSecondLine)
		{
			//Upgrading the save varriables' value and check wheather the board has a clean space there.
			saveFirstLine--;
			saveFirstRow--;
			string command = "";
			command = command + saveFirstLine;
			command = command + saveFirstRow;
			cout << command << endl;
			currentPiece = boardPtr.getBoardPieceByMoveCommand(command); //Sending to the function in order to get the piece.
			if (currentPiece->color == this->color)
			{
				return false;
			}
			if (currentPiece->type != "Empty")
			{
				return false;
			}
		}
	}
	return true;
}