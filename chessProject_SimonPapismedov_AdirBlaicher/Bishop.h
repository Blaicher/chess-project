#pragma once
#include "Board.h"
#include "Piece.h"
class Bishop : public Piece
{
public:
	Bishop(char color);
	virtual bool isLegal(string move, Board boardPtr) const;
	string type = "Bishop";
	char color;
	char sign = 'B';
};