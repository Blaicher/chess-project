#pragma once
#include "Game.h"
#include "Piece.h"
#include <string>
#include <iostream>
#define CHESSBOARD_SIZE 8
#define MOVE_LENGTH 4
using std::string;
class Piece;
//Defines\varriables for the function of understanding wheather a spesific square owns a good piece.
class Board 
{
public:
	Board(); // Function to fill _board
	void printBoard(void);
	bool sourceSquareIsInTheSameColor(string move,char current_col)const;
	int move(string move);
	int* getKingPosition();
	static int* getPositions(string move);
	Piece* checkPosition(int* positions) const;
	Piece* getBoardPieceByMoveCommand(string move)const;
	Board& operator=(const Board& from);
private:
	Piece* _board[CHESSBOARD_SIZE][CHESSBOARD_SIZE];
	char boardLines[8] = { 'a','b','c','d','e','f','g','h' }; //All the characters that the board has in its rows.
	void _setRooks();
	void _setKnights();
	void _setBishops();
	void _setQueens();
	void _setKings();
	
};