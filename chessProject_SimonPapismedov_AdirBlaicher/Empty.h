#pragma once
#include "Piece.h"

class Empty : public Piece
{
public:
	Empty(char color)
	{
		this->color = color;
	}

	virtual bool isLegal(string move, Board boardPtr) const  { return false; }
	char color = '#';
	string type = "Empty";
	char sign = '#';

};