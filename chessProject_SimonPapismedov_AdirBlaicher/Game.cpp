#include <iostream>
#include "Board.h"
#include "Game.h"
#include "Piece.h"
#include "Bishop.h"
#include "King.h"
#include "Rook.h"
#include "King.h"
#include "Knight.h"
#include "Queen.h"
#include "Pawn.h"

Game::Game(Board* boardPtr)
{
	this->_boardPtr = boardPtr;
}
/*
The function checks if we can get to the king which is not in our color.
input: none
output: true if yes we can and false we can't.
*/
bool Game::findKing()
{
	Board* newBoard = this->_boardPtr;
	char indexesLines[8] = {'a','b','c','d','e','f','g','h'};
	char indexesRow[8] = {'1','2','3','4','5','6','7','8'};
	int* kingPositions = _boardPtr->getKingPosition(); //Getting the king position.
	int i = 0; //Loup varriable.
	int j = 0; //Loup varriable.
	for(i = 0;  i < CHESSBOARD_SIZE; i++)
	{
		for(j = 0; j < CHESSBOARD_SIZE; j++)
		{
			string source = "";
			source = source + indexesLines[i];
			source = source + indexesRow[j];
			Piece* currentPiece = newBoard->getBoardPieceByMoveCommand(source);
			string command = "";
			command = command + source;
			string dest = "";
			dest = dest + indexesLines[kingPositions[0]];
			dest = dest + indexesRow[kingPositions[1]];
			string command = "";
			command = command + dest;
			if (currentPiece->isLegal(command, *newBoard))
			{
				return true;
			}
			command = command + source;
			string dest = "";
			dest = dest + indexesLines[kingPositions[2]];
			dest = dest + indexesRow[kingPositions[3]];
			string command = "";
			command = command + dest;
			if (currentPiece->isLegal(command, *newBoard))
			{
				return true;
			}
		}
	}
	return false;
}

