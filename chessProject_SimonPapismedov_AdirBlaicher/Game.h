#pragma once
#include "Board.h"
class Board;
class Game 
{
public:
	Game(Board* boardPtr);
	bool findKing();
	
private:
	bool _isWhitesTurn = true;
	Board* _boardPtr;
};