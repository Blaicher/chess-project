#include <iostream>
#include "Knight.h"

Knight::Knight(char color)
{
	this->color = color;
}
bool Knight::isLegal(string move, Board boardPtr) const
{
	const int fromRow = 0;
	const int toRow = 2;
	const int fromCol = 1;
	const int toCol = 3;
	int* positions = Board::getPositions(move); // Getting the positions in the array according to string 'move'
	int toPositions[2] = { positions[toRow], positions[toCol] };
	if (positions[fromRow] + 2 == positions[toRow] && positions[fromCol] + 1 == positions[toCol] ||
		positions[fromRow] + 2 == positions[toRow] && positions[fromCol] - 1 == positions[toCol] ||
		positions[fromRow] + 1 == positions[toRow] && positions[fromCol] + 2 == positions[toCol] ||
		positions[fromRow] + 1 == positions[toRow] && positions[fromCol] - 2 == positions[toCol] ||
		positions[fromRow] - 2 == positions[toRow] && positions[fromCol] + 1 == positions[toCol] ||
		positions[fromRow] - 2 == positions[toRow] && positions[fromCol] - 1 == positions[toCol] ||
		positions[fromRow] - 1 == positions[toRow] && positions[fromCol] + 2 == positions[toCol] ||
		positions[fromRow] - 1 == positions[toRow] && positions[fromCol] - 2 == positions[toCol]) // All possible positions the knight can reach from his current position
	{
		
		Piece* cur = boardPtr.checkPosition(toPositions);
		if (cur->color == this->color) // Is already occupied by another piece of the same color!
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	else
	{
		return false;
	}
}