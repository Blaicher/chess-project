#pragma once
#include "Piece.h"
#include <iostream>
class Knight : public Piece
{

public:
	Knight(char color);
	virtual bool isLegal(string move, Board boardPtr) const;
	string type = "Knight";
	char color;
	char sign = 'N';
};
