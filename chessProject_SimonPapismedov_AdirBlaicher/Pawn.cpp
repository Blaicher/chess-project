#include "Pawn.h"
#include "Board.h"
Pawn::Pawn(char color)
{
	this->color = color;
}
bool Pawn::isLegal(string move, Board boardPtr) const
{
	const int fromRow = 0;
	const int toRow = 2;
	const int fromCol = 1;
	const int toCol = 3;
	int calcPosition = 0;
	if (this->color == 'W')
	{
		
		calcPosition = 1;
	}
	else // Is a black peice!
	{
		calcPosition = -1;
	}
	int* positions = Board::getPositions(move); // Getting the positions in the array according to string 'move'
	
	
	int toPosition[2] = { positions[toRow], positions[toCol] };
	int inWay[2] = { toPosition[0] - calcPosition, toPosition[1]  };
	
	//Moving forward:
	if (positions[fromRow] + calcPosition == positions[toRow] && positions[fromCol] == positions[toCol]) // Normal movement
	{
		
		//Checking if the position is empty!
		Piece* cur = boardPtr.checkPosition(toPosition);
		if (!(cur->type.compare("Empty"))) // Place is empty!
		{
			return true;
		}
		else
		{
			
			return false;
		}
	}

	//Capturing:
	else if (positions[fromRow] + calcPosition == positions[toRow] && positions[fromCol] == positions[toCol] + calcPosition ||
		positions[fromRow] == positions[toRow] - calcPosition && positions[fromCol] == positions[toCol] - calcPosition) // Valid capture move!
	{
		Piece* cur = boardPtr.checkPosition(toPosition);
		if (cur->type.compare("Empty"))
		{
			return false;
		}
		else if (cur->color != this->color) // Cannot capture pieces of your own color!
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	//Special move first time!
	if ((positions[fromRow] + toRow == positions[toRow]) || (positions[fromRow] - toRow == positions[toRow]) && positions[fromCol] == positions[toCol] && (positions[fromRow] == 1 || positions[fromRow] == 6)) // First move of the pawn can be two squeres forward!
	{
		 if (positions[toCol] == positions[fromCol])
		 {
			 std::cout << "a";
		 }
		Piece* cur = boardPtr.checkPosition(toPosition); // Checking the needed squere..
		std::cout << "A";
		if (!(cur->type.compare("Empty"))) // Place is empty!
		{
			
			cur = boardPtr.checkPosition(inWay);
			std::cout << inWay[0] << inWay[1] << std::endl;
			if (!(cur->type.compare("Empty")))
			{
				return true; // Legal move!
			}
			else
			{
				std::cout << "Inway!" << std::endl;
				return false;
			}
		}
		else
		{
			std::cout << "Doesn't Fit" << std::endl;
			return false;
		}
	}


	
	else
	{
		
		return false;
	}
}