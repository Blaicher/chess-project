#pragma once
#include "Piece.h"
class Pawn : public Piece
{
public:
	Pawn(char color);
	virtual bool isLegal(string move, Board boardPtr) const;
	string type = "Pawn";
	char color;
	char sign = 'P';

};