#include "Piece.h"
#include <iostream>

/*
The function changes the charactrestics of the piece object using the parameters it gets.
input: the new color,sign and type.
output: none.
*/
void Piece::setPiece(char color, char sign, string type)
{
	this->color = color;
	this->sign = sign;
	this->type = type;
}
/*
The function gets a move that the user wants to do and checks if it is on 
the llegel places in the board.
input: the move string.
output: llegel or not.
*/
bool Piece::isOnLegelIndex(string move) const
{
	//Getting the details into different varriables.
	char source_line = move[SRC_LINE];
	char dst_line = move[DST_LINE];
	char source_row = move[SRC_ROW];
	char dst_row = move[DST_ROW];
	//Checking if all the details are okay.
	if (source_line < 'a' || source_line > 'h')
	{
		return false;
	}
    if (dst_line < 'a' || dst_line > 'h')
	{
		return false;
	}
	if (source_row < '1' || source_row > '8')
	{
		return false;
	}
	if (dst_row < '1' || dst_row > '8')
	{
		return false;
	}
	return true;
}


