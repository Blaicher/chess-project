#pragma once
#include <string>
#include "Board.h"
//Defines for the indexes we are going to use when analyzing the move string:
#define SRC_LINE 0
#define DST_LINE 2
#define SRC_ROW  1
#define DST_ROW  3
//
class Board;
using std::string;
class Piece
{
public:
	virtual bool isLegal(string move, Board boardPtr) const = 0;
	void setPiece(char color, char sign, string type);
	bool isOnLegelIndex(string move) const;
	string type;
	char color;
	char sign;
};