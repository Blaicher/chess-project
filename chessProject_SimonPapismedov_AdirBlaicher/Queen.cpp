#include "Queen.h"

Queen::Queen(char color)
{
	this->color = color;
}
bool Queen::isLegal(string move, Board boardPtr) const
{
	//Start checking if the move is llegel.
	bool isInTheBoard = this->isOnLegelIndex(move); //Checking if the move is one the good places of the board.
	if (!isInTheBoard)
	{
		return false;
	}
	if (boardPtr.sourceSquareIsInTheSameColor(move, this->color) == false)
	{
		return false;
	}
}