#pragma once
#include "Piece.h"
class Queen : public Piece
{
public:
	Queen(char color);
	virtual bool isLegal(string move, Board boardPtr) const;
	string type = "Queen";
	char color;
	char sign = 'Q';
};