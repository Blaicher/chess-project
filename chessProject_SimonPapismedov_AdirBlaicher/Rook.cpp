#include "Rook.h"
#include <iostream>
using namespace std;

Rook::Rook(char color)
{
	this->color = color;
}

bool Rook::isLegal(string move, Board boardPtr) const
{
	//Varriable to save the current piece we are dealling with:
	Piece* currentPiece = 0;
	//Getting the information of the move command.
	char firstLine = move[0];
	char secondLine = move[2];
	char firstRow = move[1];
	char secondRow = move[3];
	//Helping varriables to use in the future.
	char saveSecondLine = secondLine;
	char saveFirstLine = firstLine;
	//
	string new_move = ""; //Keeps the second half string.
	//Start checking if the move is llegel.
	bool isInTheBoard = this->isOnLegelIndex(move); //Checking if the move is one the good places of the board.
	if (!isInTheBoard)
	{
		return false;
	}
	//Getting the new move string and sending to the function for checking if the destination square is not our color type of piece.
	new_move = move[2] + move[3];
	if (boardPtr.sourceSquareIsInTheSameColor(new_move, this->color) == true)
	{
		return false;
	}
	//Saving the char values of the rows as integers.
	int firstRowIntValue = firstRow - '0';
	int secondRowIntValue = secondRow - '0';
	//Checking if the movement command of the rook is okay.
	if ((firstRowIntValue == secondRowIntValue) && (firstLine == secondLine))
	{
		return false;
	}
	///Checking if there is a clean path for the rook to move.
	if (secondLine > firstLine && firstRowIntValue == secondRowIntValue)
	{
		while (secondLine > firstLine)
		{
			firstLine++;
			string command = "";
			command = command +firstLine;
			command = command + firstRow;
			currentPiece = boardPtr.getBoardPieceByMoveCommand(command);
			//Checking if the piece we got to is fine.
			if (currentPiece->color == this->color)
			{
				return false;
			}
			if (currentPiece->type != "Empty")
			{
				return false;
			}
		}
	}
	else if (secondLine < firstLine && firstRowIntValue == secondRowIntValue)
	{
		while (firstLine != secondLine) {
			firstLine--;
			string command = command + firstLine;
			command = command + firstRow;
			currentPiece = boardPtr.getBoardPieceByMoveCommand(command);
			//Checking if the piece we got to is fine.
			if (currentPiece->color == this->color)
			{
				return false;
			}
			if (currentPiece->type != "Empty")
			{
				return false;
			}
		}
	}
	//Contuing to check.
	if (firstRowIntValue > secondRowIntValue)
	{
		while (firstRow != secondRow) {
			firstRow--;
			string command = "";
			command = firstLine;
			command = command + firstRow;
			currentPiece = boardPtr.getBoardPieceByMoveCommand(command);
			//Checking if the piece we got to is fine.
			if (currentPiece->color == this->color)
			{
				return false;
			}
			if (currentPiece->type != "Empty")
			{
				return false;
			}
		}
	}
	else if (secondRowIntValue > firstRowIntValue)
	{
		while (firstRow != secondRow) {
			cout << "Entered: ";
			firstRow++;
			string command = "";
			command = firstLine;
			command = command + firstRow;
			currentPiece = boardPtr.getBoardPieceByMoveCommand(command);
			//Checking if the piece we got to is fine.
			if (currentPiece->color == this->color)
			{
				return false;
			}
			if (currentPiece->type != "Empty")
			{
				return false;
			}
		}
	}
	return true;
}