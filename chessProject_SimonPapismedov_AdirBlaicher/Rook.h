#pragma once
#include "Piece.h"
class Rook : public Piece
{
public:
	Rook(char color);
	virtual bool isLegal(string move, Board boardPtr) const;
	
	string type = "Rook";
	char color;
	char sign = 'R';
};